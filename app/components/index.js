import React, { Component } from 'react';

import CryptocurrencyPanel from './CryptocurrencyPanel';
import CryptocurrencyCalculator from './CryptocurrencyCalculator';
import CryptocurrencyChart from './CryptocurrencyChart';
import CryptocurrencyChartLive from './CryptocurrencyChartLive';

export default class App extends Component {
  render() {
    return (
      <div>
        <CryptocurrencyPanel />
        <CryptocurrencyCalculator />
        <CryptocurrencyChart />
        <CryptocurrencyChartLive />
      </div>
    )
  }
}