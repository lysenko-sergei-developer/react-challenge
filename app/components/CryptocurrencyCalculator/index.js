import React, { Component } from 'react'

export default class CryptocurrencyCalculator extends Component {
  state = {
    cryptoData: [],
    value: '',
    inputCurrencyName: 'bitcoin',
    cryptoList: [],
    outputValue: null,
    currentСurrency: {
      name: 'EUR',
      objectID: 'price_eur'
    }
  }

  componentWillMount() {
    this.getCryptoData()
    this.getCryptoList()
  }

  getCryptoList = () => {
    fetch(`https://api.coinmarketcap.com/v1/ticker/?limit=6`)
      .then(j => j.json())
      .then(d => d.map(d => d.name))
      .then(names => this.setState({
        cryptoList: names
      }))
  }

  getCryptoFromApi = (inputCurrencyName, currency) => 
    fetch(`https://api.coinmarketcap.com/v1/ticker/${inputCurrencyName}/?convert=${currency.name}`).
      then(j => j.json())

  getCryptoData = (inputCurrencyName = this.state.inputCurrencyName, currency = this.state.currentСurrency) => {
    this.getCryptoFromApi(inputCurrencyName, currency)
      .then(d => {

        this.setState({
          cryptoData:  d,
          inputCurrencyName,
          currentСurrency: currency,
          outputValue: d[0][`${currency.objectID}`]*this.state.value
        })
      })
  }

  handlerChangeCurrency = (event) => {
    const nextCurrentCurrency = {
      name: event.target.value,
      objectID: `price_${event.target.value.toLowerCase()}`
    }

    this.getCryptoData(this.state.inputCurrencyName, nextCurrentCurrency)
  }

  handlerChangeCryptocurrency = (event) => {

    this.getCryptoData(event.target.value
      .toLowerCase()
      .replace(' ', '-')
    )
  }

  renderSelectCryptocurrency = () => {
    return(
      <select 
        defaultValue='Bitcoin'
        className="CryptocurrencyPanel__select"
        onChange={e => this.handlerChangeCryptocurrency(e)}
      >
        {this.state.cryptoList.map((cur, id) => 
          <option 
            key={`cur_${id}`}
            className='CryptocurrencyPanel__option'
          >
            {cur}
          </option>)
        }
      </select>
    )
  }

  renderSelectCurrency = () => {
    const currency = ['USD', 'EUR', 'RUB']
    return(
      <select 
        defaultValue='EUR'
        className="CryptocurrencyPanel__select"
        onChange={e => this.handlerChangeCurrency(e)}
      >
        {currency.map((cur, id) => 
          <option 
            key={`cur_${id}`}
            className='CryptocurrencyPanel__option'
          >
            {cur}
          </option>)
        }
      </select>
    )
  }

  handlerSetValue = (e) => {
    this.setState({
      value: e.target.value,
    })
  }

  render() {
    return(
      <div className="CryptocurrencyCalculator">
        {
          this.state.cryptoData ?

          <div className="CryptocurrencyCalculator__content">
          <h2 className="CryptocurrencyCalculator__title">
            Cryptocurrency Converter Calculator
          </h2>

        <div className="CryptocurrencyCalculator__container">
          <div className="CryptocurrencyCalculator__insideContainer">
            <input 
              className="CryptocurrencyCalculator__input"
              onChange={this.handlerSetValue}
              type="number"
              min="0"
            >
            </input>
            {this.renderSelectCryptocurrency()}
            <p>
              {this.state.inputCurrencyName ? 
                  `${this.state.value} ${this.state.inputCurrencyName}` : ''
              }
            </p>
          </div>

          <div className="CryptocurrencyCalculator__insideContainer">
           <p>{`<->`}</p>
          </div>

          <div className="CryptocurrencyCalculator__insideContainer">
          {this.renderSelectCurrency()}
           <p>
            {this.state.outputValue ? 
              `${this.state.outputValue}` : ''
            }
          </p>
          </div>
           
        </div>
        </div>
        : 'Spiner'}
      </div>
    )
  }
}