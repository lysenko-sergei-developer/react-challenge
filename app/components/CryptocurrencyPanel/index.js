import React, { Component } from 'react'

export default class CryptocurrencyPanel extends Component {
  componentWillMount() {
    this.getCryptoData()
  }

  getCrutpoFromApi = (currency = 'EUR') => 
    fetch(`https://api.coinmarketcap.com/v1/ticker/?convert=${currency}&limit=6`).
      then(j => j.json())

  getCryptoData = (currency = this.state.currentСurrency) => {
    this.getCrutpoFromApi(currency.name)
      .then(d => 
        this.setState({
          cryptoData:  d,
          currentСurrency: currency,
        })
      )
  }

  state = {
    currentСurrency: {
      name: 'EUR',
      objectID: 'price_eur'
    },
    cryptoData: [],
  }

  handlerChangeCurrency = (event) => {
    const nextCurrentCurrency = {
      name: event.target.value,
      objectID: `price_${event.target.value.toLowerCase()}`
    }

    this.getCryptoData(nextCurrentCurrency)
  }

  renderSelect = () => {
    const currency = ['USD', 'EUR', 'RUB']
    
    return(
      <select 
        defaultValue='EUR'
        className="CryptocurrencyPanel__select"
        onChange={e => this.handlerChangeCurrency(e)}
      >
        {currency.map((cur, id) => 
          <option 
            key={`cur_${id}`}
            className='CryptocurrencyPanel__option'
          >
            {cur}
          </option>)
        }
      </select>
    )
  }

  renderCryptoStats = () => {
    return(
      <div className="CryptocurrencyPanel__cryptoPanel">
      {
        this.state.cryptoData.map((crypto, id) => 
          <div
            key={`crypto_${id}`} 
            className="CryptocurrencyPanel__crypto">
            <p className="CryptocurrencyPanel__cryptoName">{crypto.name}</p>
            <p className="CryptocurrencyPanel__cryptoPrice">{crypto[`${this.state.currentСurrency.objectID}`]}</p>
          </div>)
      }
      </div>
    )
  }

  render() {
    return(
      <div>
        { this.state.cryptoData ?
        <div className="CryptocurrencyPanel">
          {this.renderSelect()}
          {this.renderCryptoStats()}
        </div>
        : 'Spiner'}
    </div>
    )
  }
}