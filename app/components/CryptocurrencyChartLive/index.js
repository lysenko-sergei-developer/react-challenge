import React, { Component } from 'react'
import {
  Line
} from 'react-chartjs-2'
import Websocket from 'react-websocket'

export default class CryptocurrencChartLive extends Component {

  state = {
    cryptoDataValues: [],
  }

  handlerLiveData = (data) => {
    const nextCryptoDataValues = [...this.state.cryptoDataValues]
    
    if (nextCryptoDataValues.length > 20) {
      nextCryptoDataValues.shift()
      nextCryptoDataValues.push(data);
    } else {
      nextCryptoDataValues.push(data);
    }

    this.setState({
      cryptoDataValues: nextCryptoDataValues,
    })
  }
  
  updateChart = (e) => {
    const data = JSON.parse(e).events[0].price

    this.handlerLiveData(data)
  }


  render() {
    return(
        <div className="CryptocurrencyChart">
        { this.state.cryptoDataValues ?
        <div className="CryptocurrencyChart__container"> 
         <Websocket url='wss://api.gemini.com/v1/marketdata/btcusd'
          onMessage={this.updateChart}/>

          <Line
            positionType={'bottom'}
            legend={{
              position: 'bottom',
              onHover	: () => {},
            }}
            data={{
            labels: this.state.cryptoDataValues,
            datasets: [
              {
                label: 'BTC',
                fill: false,
                lineTension: 0.1,
                backgroundColor: 'rgba(75,192,192,0.4)',
                borderColor: 'rgba(75,192,192,1)',
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderWidth: 1,
                pointHoverRadius: 30,
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.state.cryptoDataValues,
              }
            ],
          }}/>
          </div>
          : 'Spiner' }
      </div>
      
    )
  }
}