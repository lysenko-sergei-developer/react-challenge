import React, { Component } from 'react';
import {
  Line
} from 'react-chartjs-2';

export default class CryptocurrencChart extends Component {

  state = {
    currentSet: 'Month',
    cryptoDataValues: [],
    cryptoDataLabels: []
  }

  componentWillMount() {
    this.handlerChangeSet();
  }

  getCryptoFromApi = (prevTime, now) => 
    fetch(`https://api.coindesk.com/v1/bpi/historical/close.json?start=${prevTime}&end=${now}`)
      .then(j => j.json())

  getCryptoData = (prevTime, now) => {
    this.getCryptoFromApi(prevTime, now)
      .then(d => {

        const cryptoDataLabels = Object.keys(d.bpi).map(i => i);
        const cryptoDataValues = Object.values(d.bpi).map(i => i);

        this.setState({
          cryptoDataLabels: cryptoDataLabels,
          cryptoDataValues: cryptoDataValues
        })
    })
  }

  getTime = () => new Date().toISOString().slice(0, 10);

  getDataFromMonth = (now) => {
    const integerMonth = +now.slice(5,7) - 1;
    const prevMonthString = integerMonth > 11 ? integerMonth.toString() : '0' + integerMonth.toString();

    return integerMonth >= 1 ? (now.slice(0,4) +'-'+ prevMonthString +'-'+  now.slice(8, 10)) :
      (now.slice(0,4) +'-'+  '12' +'-'+  now.slice(8, 10));
  }

  getDataFromYear = (now) => {
    const nowYear = now.slice(0,4);
    const integerYear = +now.slice(0,4) - 1;
    
    return now.replace(nowYear, integerYear.toString())
  }

  handlerChangeSet = (e) => {
    const set = e ? e.target.value : 'default'
    const now = this.getTime();
    
    switch(set) {
      case 'Month':
        this.getCryptoData(this.getDataFromMonth(now), now);
        break;

      case 'Year':
        this.getCryptoData(this.getDataFromYear(now), now);
        break;

      default:

        this.getCryptoData(this.getDataFromMonth(now), now);
        break;
    }
  }

  renderSelect = () => {
    const data = ['Month', 'Year'];

    return(
      <select 
        defaultValue='Month'
        className="CryptocurrencyChart__select"
        onChange={e => this.handlerChangeSet(e)}
      >
        {data.map((set, id) => 
          <option 
            key={`set_${id}`}
            className='CryptocurrencyPanel__option'
          >
            {set}
          </option>)
        }
      </select>
    )
  }

  render() {
    return(
        <div className="CryptocurrencyChart">
        { this.state.cryptoDataValues ?
        <div className="CryptocurrencyChart__container"> 
          <div className="CryptocurrencyChart__selectContainer">
            {this.renderSelect()}
          </div>

          <Line
            positionType={'bottom'}
            legend={{
              position: 'bottom',
            }}
            data={{
            labels: this.state.cryptoDataLabels,
            
            datasets: [
              {
                label: 'BTC',
                fill: false,
                lineTension: 0.1,
                backgroundColor: 'rgba(75,192,192,0.4)',
                borderColor: 'rgba(75,192,192,1)',
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderWidth: 1,
                pointHoverRadius: 30,
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.state.cryptoDataValues
              }
            ],
          }}/>
          </div>
          : 'Spiner' }
      </div>
      
    )
  }
}